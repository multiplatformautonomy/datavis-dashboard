FROM rocker/shiny-verse:latest

RUN R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('shinydashboard', repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('ggplot2', repos='http://cran.rstudio.com/')"
RUN R -e "install.packages('tidyverse', repos='http://cran.rstudio.com/')"

# RUN  rm -rf /srv/shiny-server/*
RUN  mkdir -p /srv/shiny-server/datavis/data


COPY datavis-dashboard.Rproj /srv/shiny-server/datavis
COPY Dashboard.R /srv/shiny-server/datavis
COPY faux2.csv /srv/shiny-server/datavis/data

EXPOSE 3838


RUN chown -R shiny:shiny /srv/shiny-server


CMD ["/usr/bin/shiny-server"]